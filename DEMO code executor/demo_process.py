from CodeChecker.spell_checker import check_syntax
from CodeMerger.code_merger import merge_code

path = 'CodeMerger/test/'

merge_code(path+'merged.py', [path+'context.py', path+'tests.py'])
check_syntax(path+'merged.py')
