import os
import sys
from subprocess import Popen, PIPE, STDOUT

Popen([sys.executable, '-u', '../bin/code_merger.py'])

#script_path = os.path.join(get_script_dir(), 'pattern.py')
p = Popen([sys.executable, '-u', 'merged.py'],
          stdout=PIPE, stderr=STDOUT, bufsize=1)
with p.stdout:
    for line in iter(p.stdout.readline, b''):
        print(line),
p.wait()

#def get_script_dir():
