def __init__():
    pass

def merge_code(merge_path, filenames = ['../test/context.py', '../test/tests.py']):
    with open(merge_path, 'w') as outfile:
        for fname in filenames:
            with open(fname) as infile:
                for line in infile:
                    outfile.write(line)
